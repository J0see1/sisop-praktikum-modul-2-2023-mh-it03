#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <time.h>
#include <limits.h>

#define LOG_FILE "cleaner.log"
#define SUSPICIOUS_STRING "SUSPICIOUS"
#define INTERVAL 30

//memberikan format penamaan pada file log, serta pesan yang akan diberikan
void log_message(const char *message, const char *logPath) {
    FILE *logFile = fopen(logPath, "a");
    if (logFile == NULL) {
        perror("tidak dapat membuka file log");
        exit(EXIT_FAILURE);
    }

    time_t now;
    struct tm tm_info;
    time(&now);
    localtime_r(&now, &tm_info);
    
    char formatted_time[64];
    strftime(formatted_time, sizeof(formatted_time), "[%Y-%m-%d %H:%M:%S]", &tm_info);
    fprintf(logFile, "%s %s\n", formatted_time, message);

    fclose(logFile);
}


void remove_suspicious_files(const char *path) {
    DIR *dir = opendir(path);
    if (dir == NULL) {
        perror("Error opening directory");
        exit(EXIT_FAILURE);
    }
// membuat struct pada data entry yang akan dieksekusi
    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {  // cek apakah itu file reguler
            char file_path[PATH_MAX];
            snprintf(file_path, sizeof(file_path), "%s/%s", path, entry->d_name);

            FILE *file = fopen(file_path, "r");
            if (file != NULL) {
                char buffer[1024];
                while (fgets(buffer, sizeof(buffer), file) != NULL) {
                    if (strstr(buffer, SUSPICIOUS_STRING) != NULL) {
                        remove(file_path);
                        log_message(logFIle, file_path);
                        break;
                    }
                }
                fclose(file);
            }
        }
    }

    closedir(dir);
}

int main(int argc, char *argv[]) {
    if (argc != 2) { //memastikan bahwa program menerima satu argumen yang merupakan path directory yang akan di scan
        fprintf(stderr, "Usage: %s </home/dave/cleaner>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    const char *directory_path = argv[1];

    // membuat process menjadi daemon
    if (daemon(0, 0) == -1) {
        perror("Error daemonizing the process");
        exit(EXIT_FAILURE);
    }

    // looping process daemon
    while (1) {
        remove_suspicious_files(directory_path);
        sleep(INTERVAL);
    }

    return EXIT_SUCCESS; //keluar dari program setelah loop daemon selesai
}
