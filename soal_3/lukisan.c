#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <dirent.h>
#include <sys/wait.h>
#include <signal.h>


#define MAX_FOLDERS 5
#define MAX_IMAGES 15
#define IMAGE_WIDTH_HEIGHT 200
#define INTERVAL_FOLDER 30
#define INTERVAL_IMAGE 5


// Fungsi untuk membuat folder dengan timestamp
void createFolder(char *folder_name) {
   if (mkdir(folder_name, 0777) == -1) {
       perror("mkdir");
       exit(1);
   }
   printf("Created folder: %s\n", folder_name);
}


// Fungsi untuk mengunduh gambar dari URL dan menyimpannya dengan timestamp
void downloadImage(char *folder_name, int count) {
   time_t current_time;
   struct tm *time_info;
   char timestamp[20];
   char image_url[100];
   char image_name[100];


   time(&current_time);
   time_info = localtime(&current_time);


   strftime(timestamp, sizeof(timestamp), "%Y-%m-%d_%H:%M:%S", time_info);
   snprintf(image_name, sizeof(image_name), "%s/%s_%d.jpg", folder_name, timestamp, count);


   // Generate URL sesuai dengan timestamp
   snprintf(image_url, sizeof(image_url), "https://source.unsplash.com/%dx%d/?random", IMAGE_WIDTH_HEIGHT, IMAGE_WIDTH_HEIGHT);


   // Unduh gambar dan simpan dengan nama timestamp
   char *wget_args[] = { "wget", "-q", "-O", image_name, image_url, NULL };
   pid_t wget_pid = fork();
   if (wget_pid == 0) {
       if (execvp("wget", wget_args) == -1) {
           perror("execvp");
           exit(1);
       }
   } else if (wget_pid < 0) {
       perror("fork");
       exit(1);
   }
   int wget_status;
   waitpid(wget_pid, &wget_status, 0);
   if (WEXITSTATUS(wget_status) != 0) {
       // Handle the error here
       printf("Error: Image not found (404)\n");
       exit(1);
   }
}


// Fungsi untuk mengompres folder dan menghapusnya
void zipAndDeleteFolder(char *folder_name) {
   char zip_command[] = "zip";
   char zip_args[] = "-rq";
   char zip_file[100];
   snprintf(zip_file, sizeof(zip_file), "%s.zip", folder_name);


   pid_t zip_pid = fork();
   if (zip_pid == 0) {
       if (execlp(zip_command, zip_command, zip_args, zip_file, ".", NULL) == -1) {
           perror("execlp");
           exit(1);
       }
   } else if (zip_pid < 0) {
       perror("fork");
       exit(1);
   }
   int zip_status;
   waitpid(zip_pid, &zip_status, 0);
   if (WEXITSTATUS(zip_status) != 0) {
       perror("zip");
       exit(1);
   }


   // Hapus folder yang sudah di-zip
   char remove_command[100];
   snprintf(remove_command, sizeof(remove_command), "rm -r %s", folder_name);
   pid_t remove_pid = fork();
   if (remove_pid == 0) {
       if (execlp("rm", "rm", "-r", folder_name, NULL) == -1) {
           perror("execlp");
           exit(1);
       }
   } else if (remove_pid < 0) {
       perror("fork");
       exit(1);
   }
   int remove_status;
   waitpid(remove_pid, &remove_status, 0);
   if (WEXITSTATUS(remove_status) != 0) {
       perror("rm");
       exit(1);
   }


   printf("Zipped and deleted folder: %s\n", folder_name);
}


// Fungsi untuk mengompres semua folder yang ada ke dalam satu file zip
void zipAllFolders() {
   char zip_command[] = "zip";
   char zip_args[] = "-rq";
   char zip_file[] = "all_folders.zip";


   pid_t zip_pid = fork();
   if (zip_pid == 0) {
       if (execlp(zip_command, zip_command, zip_args, zip_file, "*", NULL) == -1) {
           perror("execlp");
           exit(1);
       }
   } else if (zip_pid < 0) {
       perror("fork");
       exit(1);
   }
   int zip_status;
   waitpid(zip_pid, &zip_status, 0);
   if (WEXITSTATUS(zip_status) != 0) {
       perror("zip");
       exit(1);
   }


   printf("Zipped all folders into: all_folders.zip\n");
}


// Fungsi untuk menangani sinyal SIGTERM
void handleSIGTERM(int signum) {
   printf("Received SIGTERM signal. Terminating program...\n");
   // Tambahkan tindakan penutupan yang diperlukan di sini
   exit(0);
}


// Fungsi untuk membuat program "killer"
void createKiller(char *program_name) {
   char killer_code[] = "#include <stdio.h>\n"
                       "#include <stdlib.h>\n"
                       "#include <signal.h>\n"
                       "#include <unistd.h>\n"
                       "\n"
                       "void handleSIGTERM(int signum) {\n"
                       "    printf(\"Received SIGTERM signal. Terminating program...\\n\");\n"
                       "    kill(getppid(), SIGTERM); // Mengirim SIGTERM ke parent process (program utama)\n"
                       "    remove(\"killer\"); // Menghapus file killer setelah digunakan\n"
                       "}\n"
                       "\n"
                       "int main() {\n"
                       "    signal(SIGTERM, handleSIGTERM); // Menangani sinyal SIGTERM\n"
                       "    printf(\"Killer program started. Waiting for instructions...\\n\");\n"
                       "    while (1) {\n"
                       "        sleep(1); // Program killer berjalan dan menunggu instruksi\n"
                       "    }\n"
                       "    return 0;\n"
                       "}\n";


   // Simpan kode program "killer" ke dalam file "killer.c"
   FILE *killer_file = fopen("killer.c", "w");
   if (killer_file == NULL) {
       perror("fopen");
       exit(1);
   }
   fprintf(killer_file, "%s", killer_code);
   fclose(killer_file);


   // Kompilasi program "killer" menjadi executable
   pid_t compile_pid = fork();
   if (compile_pid == 0) {
       if (execlp("gcc", "gcc", "-o", program_name, "killer.c", NULL) == -1) {
           perror("execlp");
           exit(1);
       }
   } else if (compile_pid < 0) {
       perror("fork");
       exit(1);
   }


   int compile_status;
   waitpid(compile_pid, &compile_status, 0);
   if (WEXITSTATUS(compile_status) != 0) {
       perror("gcc");
       exit(1);
   }


   printf("Program \"%s\" created successfully.\n", program_name);
}


// Fungsi untuk menjadikan program sebagai daemon
void daemonize() {
   // Lakukan fork pertama
   pid_t pid = fork();
   if (pid < 0) {
       perror("fork");
       exit(1);
   }
   if (pid > 0) {
       exit(0); // Keluar dari parent process
   }


   // Lakukan fork kedua untuk menjadi sesi pemimpin (session leader)
   pid = fork();
   if (pid < 0) {
       perror("fork");
       exit(1);
   }
   if (pid > 0) {
       exit(0); // Keluar dari parent process
   }


   // Mengubah direktori kerja ke direktori root ("/")
   chdir("/");


   // Menutup file descriptor standar
   close(STDIN_FILENO);
   close(STDOUT_FILENO);
   close(STDERR_FILENO);
}


int main(int argc, char *argv[]) {
   if (argc != 2 || (strcmp(argv[1], "-a") != 0 && strcmp(argv[1], "-b") != 0)) {
       printf("Gunakan argumen -a untuk MODE_A atau -b untuk MODE_B.\n");
       exit(1);
   }
   int mode_a = (strcmp(argv[1], "-a") == 0);


   // Menjadi daemon
   daemonize();


   // Mengehandle sinyal SIGTERM (seperti sebelumnya)
   signal(SIGTERM, handleSIGTERM);


   // Buat program "killer" seperti sebelumnya
   createKiller("killer");


   int folder_count = 0;
   int image_count = 0;


   while (1) {
       char folder_name[50];
       time_t current_time;
       struct tm *time_info;
       char timestamp[20];


       time(&current_time);
       time_info = localtime(&current_time);


       strftime(timestamp, sizeof(timestamp), "%Y-%m-%d_%H:%M:%S", time_info);
       snprintf(folder_name, sizeof(folder_name), "%s", timestamp);


       createFolder(folder_name);


       for (int i = 0; i < MAX_IMAGES; i++) {
           downloadImage(folder_name, image_count);
           image_count++;
           sleep(INTERVAL_IMAGE);
       }


       folder_count++;
       image_count = 0;


       if (folder_count >= MAX_FOLDERS) {
           if (!mode_a) {
               zipAllFolders(); // Panggil fungsi zipAllFolders untuk mengompres semua folder
           }
           folder_count = 0;


           if (mode_a) {
               printf("MODE_A: Program utama dihentikan karena program killer dijalankan.\n");
               break;
           }
       } else {
           zipAndDeleteFolder(folder_name); // Di-zip segera setelah folder selesai diisi gambar
       }
       sleep(INTERVAL_FOLDER);
   }
   return 0;
}