
# Project Title

A brief description of what this project does and who it's for


# sisop-praktikum-modul-2-2023-mh-it03

Pengerjaan soal shift sistem operasi modul 2 oleh IT03

# Anggota

| Nama                            | NRP          |
| ------------------------------- | ------------ |
| Marcelinus Alvinanda Chrisantya | `5027221012` |
| George David Nebore             | `5027221043` |
| Angella Christie                | `5027221047` |

# Konten

- [Soal 1](#soal-1)

- [Soal 2](#soal-2)

- [Soal 3](#soal-3)

- [Soal 4](#soal-4)

# Soal 1


# Program Cleaner

John adalah seorang mahasiswa biasa. Pada tahun ke-dua kuliahnya, dia merasa bahwa dia telah menyianyiakan waktu kuliahnya selama ini. Selama mengerjakan tugas, John selalu menggunakan bantuan ChatGPT dan tidak pernah mempelajari apapun dari hal tersebut. Untuk itu, pada soal kali ini John bertekad untuk tidak menggunakan ChatGPT dan mencoba menyelesaikan tugasnya dengan tangan dan pikirannya sendiri. Melihat tekad yang kuat dari John, Mark, dosen yang mengajar John, ingin membantunya belajar dengan memberikan sebuah ujian. Sebelum memberikan ujian pada John, Mark berpesan bahwa John harus bersungguh-sungguh dalam mengerjakan ujian, fokus untuk belajar, dan tidak perlu khawatir akan nilai yang diberikan. Mark memberikan ujian pada John untuk membuatkannya sebuah program `cleaner` sederhana dengan ketentuan berikut :

1. Program tersebut menerima input path dengan menggunakan “argv”.
2. Program tersebut bertugas untuk menghapus file yang didalamnya terdapat string "SUSPICIOUS" pada direktori yang telah diinputkan oleh user.
3. Program tersebut harus berjalan secara daemon.
Program tersebut akan terus berjalan di background dengan jeda 30 detik.
4. Dalam pembuatan program tersebut, tidak diperbolehkan menggunakan fungsi `system()`.
5. Setiap kali program tersebut menghapus sebuah file, maka akan dicatat pada file 'cleaner.log' yang ada pada direktori home user dengan format seperti berikut : “[YYYY-mm-dd HH:MM:SS] '<absolute_path_to_file>' has been removed.”.

```C
## Cleaner.c 

    #include <stdio.h>  
    #include <stdlib.h>  
    #include <string.h>  
    #include <unistd.h>  
    #include <dirent.h>  
    #include <time.h>  
    #include <syslog.h>  
    #include <sys/types.h>  
    #include <sys/stat.h>  
    #include <signal.h>  

    int should_sleep = 1;


    void write_log(const char *file_path) {
        time_t raw_time;
        struct tm *time_info;
        char time_str[20];
        time(&raw_time);
        time_info = localtime(&raw_time);
        strftime(time_str, sizeof(time_str), "%Y-%m-%d %H:%M:%S", time_info);

    char log_entry[256];
    snprintf(log_entry, sizeof(log_entry), "[%s] '%s' has been removed.\n", time_str, file_path);

    openlog("cleaner", LOG_PID | LOG_NDELAY | LOG_NOWAIT | LOG_CONS, LOG_USER);
    syslog(LOG_INFO, "%s", log_entry);
    closelog();
    }

    void daemonize() {
    pid_t pid;

    pid = fork();
    if (pid < 0) {
        perror("Fork failed");
        exit(EXIT_FAILURE);
    }

    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    if (setsid() < 0) {
        perror("setsid failed");
        exit(EXIT_FAILURE);
    }

    if (chdir("/") < 0) {
        perror("chdir failed");
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
    }

    void handle_signal(int signo) {
        if (signo == SIGTERM) {
            // Tambahkan tindakan yang sesuai saat menerima SIGTERM (penghentian daemon)
            // Contoh: Menutup file log, membersihkan sumber daya, dll.
            closelog();
            exit(EXIT_SUCCESS);
        }
    }

    void search_directory(const char *dir_path) {
    // Sisipkan kode pencarian direktori seperti yang Anda lakukan sebelumnya
    // Pastikan tindakan yang sesuai diambil saat menghapus file.
    }

    int main(int argc, char **argv) {
    // Tambahkan penanganan sinyal
    signal(SIGTERM, handle_signal);

    if (argc != 2) {
        printf("Usage: %s '/home/dave'\n", argv[0]);
        return 1;
    }

    // Daemonize program
    daemonize();

    while (1) {
        should_sleep = 1;
        search_directory("/home/dave/praktikum-sisop/test");

        if (should_sleep) {
            sleep(30);
        }
    }

    return 0;
    }
```


diatas merupakan program cleaner yang sudah dibuat.

```C
    #include <stdio.h>  
    #include <stdlib.h>  
    #include <string.h>  
    #include <unistd.h>  
    #include <dirent.h>  
    #include <time.h>  
    #include <syslog.h>  
    #include <sys/types.h>  
    #include <sys/stat.h>  
    #include <signal.h>  
```
Baris pertama hingga ke-12 adalah direktif preprocessor untuk mengimpor berbagai pustaka yang diperlukan.

```C
    int should_sleep = 1;
```
Variabel ini digunakan untuk mengontrol apakah program harus tidur atau tidak. Nilai awalnya diatur ke 1, dan akan diubah menjadi 0 ketika ada aktivitas yang perlu dilakukan.

    void write_log(const char *file_path) {
    // ...
    }
 Ini adalah fungsi untuk menulis entri log. Menerima path file yang dihapus, mendapatkan waktu saat ini, dan mencatat pesan log ke dalam sistem.

    void daemonize() {
    // ...
    }
Fungsi ini mengubah program menjadi daemon. Melibatkan proses fork, pengaturan session ID, perubahan direktori kerja, menutup file descriptor standar, dan beberapa langkah lainnya.

    void handle_signal(int signo) {
    // ...
    }
Fungsi ini menangani sinyal, terutama SIGTERM. Pada penerimaan sinyal SIGTERM, ini menutup log dan menghentikan daemon.

    void search_directory(const char *dir_path) {
    // ...
    }
Ini adalah fungsi yang mencari file dalam direktori yang diberikan dan menghasilkan pesan log jika file dihapus.

    int main(int argc, char **argv) {
    // ...
    }
Fungsi main adalah titik masuk utama program. Pertama, menambahkan penanganan sinyal untuk SIGTERM. Kemudian, memastikan program dijalankan dengan argumen direktori yang benar. Selanjutnya, menjalankan daemon dan secara terus menerus mencari file dalam loop utama.

selanjutnya ketika ingin mengcompile program cleaner gunakan perintah

    gcc cleaner.c -o cleaner
maka file tersebut akan tercompile dan tinggal menjalankannya dengan perintah

    ./cleaner

# Output 
 Berikut outputnya :
 
 ![WhatsApp Image 2023-10-13 at 19 10 12_5b47d86e](https://github.com/J0see1/KPP-BMS/assets/134209563/155db733-dafb-4b81-a6fa-cee7c38e1165)



# Soal 2

QQ adalah fan Cleveland Cavaliers. Ia ingin memajang kamarnya dengan poster foto roster Cleveland Cavaliers tahun 2016. Maka yang dia lakukan adalah meminta tolong temannya yang sangat sisopholic untuk membuatkannya sebuah program untuk mendownload gambar - gambar pemain tersebut. Sebagai teman baiknya, bantu QQ untuk mencarikan foto foto yang dibutuhkan QQ dengan ketentuan sebagai berikut:
- Pertama, buatlah program bernama “cavs.c” yang dimana program tersebut akan membuat folder “players”.
- Program akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip” ke dalam folder players yang telah dibuat. Lalu hapus file zip tersebut agar tidak memenuhi komputer QQ.
- Dikarenakan database yang diunduh masih data mentah. Maka bantulah QQ untuk menghapus semua pemain yang bukan dari Cleveland Cavaliers yang ada di directory.
- Setelah mengetahui nama-nama pemain Cleveland Cavaliers, QQ perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 5 proses yang berbeda. Untuk kategori folder akan menjadi 5 yaitu point guard (PG), shooting guard (SG), small forward (SF), power forward (PF), dan center (C).
- Hasil kategorisasi akan di outputkan ke file Formasi.txt, dengan berisi: 
    - PG: {jumlah pemain}
    - SG: {jumlah pemain}
    - SF: {jumlah pemain}
    - PF: {jumlah pemain}
    - C: {jumlah pemain}
- Ia ingin memajang foto pemain yang menembakkan game-winning shot pada ajang NBA Finals 2016, tepatnya pada game 7, dengan membuat folder “clutch”, yang di dalamnya berisi foto pemain yang bersangkutan.
- Ia merasa kurang lengkap jika tidak memajang foto pemain yang melakukan The Block pada ajang yang sama, Maka dari itu ditaruhlah foto pemain tersebut di folder “clutch” yang sama.

# Analisis

- Jadi untuk langkah pertama kita akan membuat directory player menggunakan command exec(), berikut fungsinya :

```C
void makeDirectory(){
pid_t mkdir = fork();
if (mkdir == 0){
    execl("/usr/bin/mkdir", "mkdir", "-p", "players", NULL);
}
}
```

- Langkah berikutnya kita akan download file yang sudah disediakan menggunakan menggunakan command exec() melalui wget, berikut fungsinya : 

```C
void downloadFile(){
    pid_t wget = fork();
    if (wget == 0){
        execl("/usr/bin/wget", "wget", "-O", "players/players.zip", "https://drive.google.com/u/0/uc?id=1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK&export=download", NULL );
    } waitpid(wget, &status,0);
}
```

- Selanjutnya kita akan melakukan unzip pada file yang sudah didownload dan setelah proses unzip selesai kita akan menghapus file zip tadi. Proses tadi menggunakan fork() di mana proses unzip berada pada child process dan remove pada parent process. Parent process akan menunggu child process selesai baru untuk menjalankan programnya, hal ini bisa dilihat dengan adanya fungsi wait(). Berikut codenya :

```C
void extractZip(){
    pid_t unzip = fork();
    if (unzip == 0) {
        execl("/usr/bin/unzip", "unzip", "players/players.zip", "-d", "players", NULL);
        perror("execl");
        exit(EXIT_FAILURE); // Tambahkan exit jika execl gagal
    }
    else if (unzip > 0) {

        waitpid(unzip, &status, 0); // Tunggu proses anak selesai
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            // Proses anak selesai dengan sukses
            remove("players/players.zip"); // Hapus berkas ZIP
        } else {
            printf("Unzip failed.\n");
        }
    }
    else {
        perror("fork");
    }
    waitpid(unzip, &status, 0);
}
```

- Setelah melakukan extract, selanjutnya akan dihapus pemain yang bukan dari Cavaliers, hal ini dilakukan dengan melakukan find pada seluruh isi folder players, dan mencocokan kata pertama pada nama file .png, apabila bukan Cavaliers akan dihapus. Berikut codenya :

```C
void removeNonCavsPlayers() {
    pid_t remove = fork();
    if (remove == 0) {
        execlp("find", "find", "players", "-type", "f", "!", "-name", "Cavaliers*", "-delete", NULL);
        perror("execlp");
        exit(EXIT_FAILURE);
    } else if (remove > 0) {
        waitpid(remove, &status, 0);
    } else {
        perror("fork");
    }
}
```

- Kemudian akan dilakukan kategorisasi pada tiap pemain Cavaliers. Akan dibuat folder dengan nama sesuai posisi pemain yang ditentukan melalui kata kedua dari nama file .png. Setelah dibuat akan dilakukan pengecekan pada tiap file di folder players, dan ketika ada file yang sesuai akan langsung dipindahkan pada folder yang sesuai. Berikut codenya :

```C
void createPGFolderAndMovePlayers() {
    pid_t mkdirPG = fork(); // Membuat proses anak
    
    if (mkdirPG == 0) {
        // Proses anak
        execl("/bin/mkdir", "mkdir", "-p", "players/PG", NULL); // Memanggil mkdir melalui exec
        perror("execlp"); // Jika exec gagal
        exit(EXIT_FAILURE);
    } else if (mkdirPG > 0) {
        // Proses induk menunggu anak selesai
        int status;
        waitpid(mkdirPG, &status, 0);
        
        // Setelah folder PG dibuat, kita pindahkan pemain dengan PG di dalam namanya
        pid_t movePG = fork(); // Membuat proses anak lagi
        if (movePG == 0) {
            // Proses anak
            execl("/bin/sh", "sh", "-c", "mv players/*PG* players/PG/", NULL); // Memanggil mv melalui exec
            perror("execlp"); // Jika exec gagal
            exit(EXIT_FAILURE);
        } else if (movePG > 0) {
            // Proses induk menunggu anak selesai
            int moveStatus;
            waitpid(movePG, &moveStatus, 0);
        } else {
            perror("fork"); // Jika fork gagal
        }
    } else {
        perror("fork"); // Jika fork gagal
    }
}
```
        - *PS. Untuk tiap posisi akan dilakukan dalam proses yang berbeda, dan tinggal mengganti tulisan PG disesuaikan dengan posisi yang ingin dipindahkan.

- Selanjutnya akan dilakukan proses penghitungan pada jumlah file di tiap folder posisi dan jumlah dari proses penghitungan akan dimasukkan pada file Formasi.txt. Berikut kodenya :

```C
void generateFormationFile() {
    int pg_count = 0, sg_count = 0, sf_count = 0, pf_count = 0, c_count = 0;

    // Loop through each position folder
    char positions[5][3] = {"PG", "SG", "SF", "PF", "C"};
    for (int i = 0; i < 5; i++) {
        DIR *d;
        struct dirent *dir;
        char position_folder[32];
        snprintf(position_folder, sizeof(position_folder), "players/%s", positions[i]);
        d = opendir(position_folder);
        if (d) {
            while ((dir = readdir(d)) != NULL) {
                if (strstr(dir->d_name, "Cavaliers") != NULL && strstr(dir->d_name, ".png") != NULL) {
                    if (i == 0) pg_count++;
                    else if (i == 1) sg_count++;
                    else if (i == 2) sf_count++;
                    else if (i == 3) pf_count++;
                    else if (i == 4) c_count++;
                }
            }
            closedir(d);
        }
    }

    FILE *formation_file = fopen("Formasi.txt", "w");
    if (formation_file) {
        fprintf(formation_file, "PG: %d\n", pg_count);
        fprintf(formation_file, "SG: %d\n", sg_count);
        fprintf(formation_file, "SF: %d\n", sf_count);
        fprintf(formation_file, "PF: %d\n", pf_count);
        fprintf(formation_file, "C: %d\n", c_count);
        fclose(formation_file);
    } else {
        perror("Error creating formation file");
    }
}
```
    - *Proses dimulai dengan membuka file posisi menggunakan looping, dan program akan membaca tiap ada kecocokan posisi kemudian program akan increment nilai dari [posisi]_count. Dan akhirnya nilai dari [posisi]_count akan dimasukkan pada file Formasi.txt

- Terakhir akan dibuat directory clutch untuk menyimpan file bernama LeBron James dan Kyrie Irving. Berikut kodenya :

```C
void createClutchFolder() {
    int create = mkdir("clutch", 0777);
    if (create != 0) {
        perror("Error creating clutch directory");
    } else {
        printf("Clutch directory created successfully.\n");
    }
}

void moveClutchPhoto() {
    char src[512];
    char dst[512];
    char *  ClutchplayerName = "Cavaliers-PG-Kyrie-Irving.png";
    snprintf(src, sizeof(src), "players/PG/%s", ClutchplayerName);
    snprintf(dst, sizeof(dst), "clutch/%s", ClutchplayerName);

    int success = rename(src, dst);
    if (success == 0) {
        printf("Moved %s to clutch folder successfully.\n", ClutchplayerName);
    } else {
        perror("Error moving clutch photo");
    }
}

void moveTheBlockPhoto() {
    char src[512];
    char dst[512];
    char *  BlockplayerName = "Cavaliers-SF-LeBron-James.png";
    snprintf(src, sizeof(src), "players/SF/%s", BlockplayerName);
    snprintf(dst, sizeof(dst), "clutch/%s", BlockplayerName);

    int success = rename(src, dst);
    if (success == 0) {
        printf("Moved %s to clutch folder successfully.\n", BlockplayerName);
    } else {
        perror("Error moving clutch photo");
    }
}
```

# output 
- Berikut output dari program di atas, dengan bentuk folder sebagai berikut : 

![Screenshot 2023-10-13 014341](https://github.com/J0see1/BMS-KPP/assets/134209563/8b3e25d4-61f4-45d6-9365-ce691a9f2a2d)

- Pada terminal terlihat bahwa proses download sudah berjalan, dan pada folder players dan clutch sudah tertera file yang sesuai.

# Soal 3


Soal 3

Albedo adalah seorang seniman terkenal dari Mondstadt. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini Albedo sedang menghadapi creativity block. Sebagai teman berkebangsaan dari Fontaine yang jago sisop, bantu Albedo untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi !
- Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].
- Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://source.unsplash.com/{widthxheight} , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].
- Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip, format nama [YYYY-mm-dd_HH:mm:ss].zip tanpa “[]”).
- Karena takut program tersebut lepas kendali, Albedo ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.
- Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).
	Catatan :
    - Tidak boleh menggunakan system()
    - Proses berjalan secara daemon
    - Proses download gambar pada beberapa folder dapat berjalan secara bersamaan (overlapping)




## Code dan Penjelasan

Berikut merupakan code yang digunakan untuk menjalankan soal nomor 3 :

```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <dirent.h>
#include <sys/wait.h>
#include <signal.h>


#define MAX_FOLDERS 5
#define MAX_IMAGES 15
#define IMAGE_WIDTH_HEIGHT 200
#define INTERVAL_FOLDER 30
#define INTERVAL_IMAGE 5


// Fungsi untuk membuat folder dengan timestamp
void createFolder(char *folder_name) {
   if (mkdir(folder_name, 0777) == -1) {
       perror("mkdir");
       exit(1);
   }
   printf("Created folder: %s\n", folder_name);
}


// Fungsi untuk mengunduh gambar dari URL dan menyimpannya dengan timestamp
void downloadImage(char *folder_name, int count) {
   time_t current_time;
   struct tm *time_info;
   char timestamp[20];
   char image_url[100];
   char image_name[100];


   time(&current_time);
   time_info = localtime(&current_time);


   strftime(timestamp, sizeof(timestamp), "%Y-%m-%d_%H:%M:%S", time_info);
   snprintf(image_name, sizeof(image_name), "%s/%s_%d.jpg", folder_name, timestamp, count);


   // Generate URL sesuai dengan timestamp
   snprintf(image_url, sizeof(image_url), "https://source.unsplash.com/%dx%d/?random", IMAGE_WIDTH_HEIGHT, IMAGE_WIDTH_HEIGHT);


   // Unduh gambar dan simpan dengan nama timestamp
   char *wget_args[] = { "wget", "-q", "-O", image_name, image_url, NULL };
   pid_t wget_pid = fork();
   if (wget_pid == 0) {
       if (execvp("wget", wget_args) == -1) {
           perror("execvp");
           exit(1);
       }
   } else if (wget_pid < 0) {
       perror("fork");
       exit(1);
   }
   int wget_status;
   waitpid(wget_pid, &wget_status, 0);
   if (WEXITSTATUS(wget_status) != 0) {
       // Handle the error here
       printf("Error: Image not found (404)\n");
       exit(1);
   }
}


// Fungsi untuk mengompres folder dan menghapusnya
void zipAndDeleteFolder(char *folder_name) {
   char zip_command[] = "zip";
   char zip_args[] = "-rq";
   char zip_file[100];
   snprintf(zip_file, sizeof(zip_file), "%s.zip", folder_name);


   pid_t zip_pid = fork();
   if (zip_pid == 0) {
       if (execlp(zip_command, zip_command, zip_args, zip_file, ".", NULL) == -1) {
           perror("execlp");
           exit(1);
       }
   } else if (zip_pid < 0) {
       perror("fork");
       exit(1);
   }
   int zip_status;
   waitpid(zip_pid, &zip_status, 0);
   if (WEXITSTATUS(zip_status) != 0) {
       perror("zip");
       exit(1);
   }


   // Hapus folder yang sudah di-zip
   char remove_command[100];
   snprintf(remove_command, sizeof(remove_command), "rm -r %s", folder_name);
   pid_t remove_pid = fork();
   if (remove_pid == 0) {
       if (execlp("rm", "rm", "-r", folder_name, NULL) == -1) {
           perror("execlp");
           exit(1);
       }
   } else if (remove_pid < 0) {
       perror("fork");
       exit(1);
   }
   int remove_status;
   waitpid(remove_pid, &remove_status, 0);
   if (WEXITSTATUS(remove_status) != 0) {
       perror("rm");
       exit(1);
   }


   printf("Zipped and deleted folder: %s\n", folder_name);
}


// Fungsi untuk mengompres semua folder yang ada ke dalam satu file zip
void zipAllFolders() {
   char zip_command[] = "zip";
   char zip_args[] = "-rq";
   char zip_file[] = "all_folders.zip";


   pid_t zip_pid = fork();
   if (zip_pid == 0) {
       if (execlp(zip_command, zip_command, zip_args, zip_file, "*", NULL) == -1) {
           perror("execlp");
           exit(1);
       }
   } else if (zip_pid < 0) {
       perror("fork");
       exit(1);
   }
   int zip_status;
   waitpid(zip_pid, &zip_status, 0);
   if (WEXITSTATUS(zip_status) != 0) {
       perror("zip");
       exit(1);
   }


   printf("Zipped all folders into: all_folders.zip\n");
}


// Fungsi untuk menangani sinyal SIGTERM
void handleSIGTERM(int signum) {
   printf("Received SIGTERM signal. Terminating program...\n");
   // Tambahkan tindakan penutupan yang diperlukan di sini
   exit(0);
}


// Fungsi untuk membuat program "killer"
void createKiller(char *program_name) {
   char killer_code[] = "#include <stdio.h>\n"
                       "#include <stdlib.h>\n"
                       "#include <signal.h>\n"
                       "#include <unistd.h>\n"
                       "\n"
                       "void handleSIGTERM(int signum) {\n"
                       "    printf(\"Received SIGTERM signal. Terminating program...\\n\");\n"
                       "    kill(getppid(), SIGTERM); // Mengirim SIGTERM ke parent process (program utama)\n"
                       "    remove(\"killer\"); // Menghapus file killer setelah digunakan\n"
                       "}\n"
                       "\n"
                       "int main() {\n"
                       "    signal(SIGTERM, handleSIGTERM); // Menangani sinyal SIGTERM\n"
                       "    printf(\"Killer program started. Waiting for instructions...\\n\");\n"
                       "    while (1) {\n"
                       "        sleep(1); // Program killer berjalan dan menunggu instruksi\n"
                       "    }\n"
                       "    return 0;\n"
                       "}\n";


   // Simpan kode program "killer" ke dalam file "killer.c"
   FILE *killer_file = fopen("killer.c", "w");
   if (killer_file == NULL) {
       perror("fopen");
       exit(1);
   }
   fprintf(killer_file, "%s", killer_code);
   fclose(killer_file);


   // Kompilasi program "killer" menjadi executable
   pid_t compile_pid = fork();
   if (compile_pid == 0) {
       if (execlp("gcc", "gcc", "-o", program_name, "killer.c", NULL) == -1) {
           perror("execlp");
           exit(1);
       }
   } else if (compile_pid < 0) {
       perror("fork");
       exit(1);
   }


   int compile_status;
   waitpid(compile_pid, &compile_status, 0);
   if (WEXITSTATUS(compile_status) != 0) {
       perror("gcc");
       exit(1);
   }


   printf("Program \"%s\" created successfully.\n", program_name);
}


// Fungsi untuk menjadikan program sebagai daemon
void daemonize() {
   // Lakukan fork pertama
   pid_t pid = fork();
   if (pid < 0) {
       perror("fork");
       exit(1);
   }
   if (pid > 0) {
       exit(0); // Keluar dari parent process
   }


   // Lakukan fork kedua untuk menjadi sesi pemimpin (session leader)
   pid = fork();
   if (pid < 0) {
       perror("fork");
       exit(1);
   }
   if (pid > 0) {
       exit(0); // Keluar dari parent process
   }


   // Mengubah direktori kerja ke direktori root ("/")
   chdir("/");


   // Menutup file descriptor standar
   close(STDIN_FILENO);
   close(STDOUT_FILENO);
   close(STDERR_FILENO);
}


int main(int argc, char *argv[]) {
   if (argc != 2 || (strcmp(argv[1], "-a") != 0 && strcmp(argv[1], "-b") != 0)) {
       printf("Gunakan argumen -a untuk MODE_A atau -b untuk MODE_B.\n");
       exit(1);
   }
   int mode_a = (strcmp(argv[1], "-a") == 0);


   // Menjadi daemon
   daemonize();


   // Mengehandle sinyal SIGTERM (seperti sebelumnya)
   signal(SIGTERM, handleSIGTERM);


   // Buat program "killer" seperti sebelumnya
   createKiller("killer");


   int folder_count = 0;
   int image_count = 0;


   while (1) {
       char folder_name[50];
       time_t current_time;
       struct tm *time_info;
       char timestamp[20];


       time(&current_time);
       time_info = localtime(&current_time);


       strftime(timestamp, sizeof(timestamp), "%Y-%m-%d_%H:%M:%S", time_info);
       snprintf(folder_name, sizeof(folder_name), "%s", timestamp);


       createFolder(folder_name);


       for (int i = 0; i < MAX_IMAGES; i++) {
           downloadImage(folder_name, image_count);
           image_count++;
           sleep(INTERVAL_IMAGE);
       }


       folder_count++;
       image_count = 0;


       if (folder_count >= MAX_FOLDERS) {
           if (!mode_a) {
               zipAllFolders(); // Panggil fungsi zipAllFolders untuk mengompres semua folder
           }
           folder_count = 0;


           if (mode_a) {
               printf("MODE_A: Program utama dihentikan karena program killer dijalankan.\n");
               break;
           }
       } else {
           zipAndDeleteFolder(folder_name); // Di-zip segera setelah folder selesai diisi gambar
       }
       sleep(INTERVAL_FOLDER);
   }
   return 0;
}
```
Berikut merupakan penjelasan dari code diatas :

```
void createFolder(char *folder_name) {
   if (mkdir(folder_name, 0777) == -1) {
       perror("mkdir");
       exit(1);
   }
   printf("Created folder: %s\n", folder_name);
}
```
1. `void createFolder(char *folder_name) {`: adalah deklarasi fungsi `createFolder` yang menerima satu argumen, yaitu `folder_name` yang merupakan pointer ke string (nama folder yang akan dibuat).

2. `if (mkdir(folder_name, 0777) == -1) {`: Fungsi `mkdir` digunakan untuk membuat direktori baru. Parameter pertamanya adalah `folder_name`, yaitu nama direktori yang akan dibuat, dan parameter kedua adalah `0777`, yang mengatur izin (permissions) untuk direktori yang dibuat. Kode `0777` menunjukkan bahwa direktori tersebut akan memiliki izin penuh (read, write, execute) untuk pemilik, grup, dan pengguna lainnya. Jika `mkdir` mengembalikan nilai `-1`, itu berarti ada kesalahan dalam pembuatan folder, dan perintah `perror("mkdir")` akan mencetak pesan kesalahan ke layar dan program akan keluar dengan status error.

3. `printf("Created folder: %s\n", folder_name);`: Jika folder berhasil dibuat, program akan mencetak pesan "Created folder:" diikuti oleh nama folder yang baru dibuat.

```
void downloadImage(char *folder_name, int count) {
   time_t current_time;
   struct tm *time_info;
   char timestamp[20];
   char image_url[100];
   char image_name[100];

   time(&current_time);
   time_info = localtime(&current_time);

   strftime(timestamp, sizeof(timestamp), "%Y-%m-%d_%H:%M:%S", time_info);
   snprintf(image_name, sizeof(image_name), "%s/%s_%d.jpg", folder_name, timestamp, count);

   snprintf(image_url, sizeof(image_url), "https://source.unsplash.com/%dx%d/?random", IMAGE_WIDTH_HEIGHT, IMAGE_WIDTH_HEIGHT);
```
1. `void downloadImage(char *folder_name, int count) {`: Ini adalah deklarasi fungsi `downloadImage` yang menerima dua argumen. `folder_name` adalah nama folder tempat gambar akan disimpan, dan `count` adalah nomor urutan gambar.
2. `time_t current_time; struct tm *time_info; char timestamp[20];`: Di sini, kita mendeklarasikan beberapa variabel yang akan digunakan untuk menghasilkan timestamp. `time_t` adalah tipe data yang digunakan untuk menyimpan waktu dalam representasi waktu Unix, `struct tm` adalah struktur yang digunakan untuk menyimpan waktu yang sudah diurai, `timestamp` adalah string yang akan digunakan untuk menyimpan timestamp hasil konversi waktu.
3. `time(&current_time); time_info = localtime(&current_time);`: Kode ini digunakan untuk mendapatkan waktu saat ini (current_time) dan mengonversinya menjadi informasi waktu yang terurai (time_info) dalam struktur `tm`. Fungsi `time` digunakan untuk mendapatkan waktu saat ini dalam format Unix timestamp, dan `localtime` digunakan untuk mengurai timestamp ini menjadi tanggal dan waktu yang lebih mudah dibaca.
4. `strftime(timestamp, sizeof(timestamp), "%Y-%m-%d_%H:%M:%S", time_info);`: Fungsi `strftime` digunakan untuk memformat informasi waktu (dalam `time_info`) ke dalam string dengan format tertentu. Format timestamp "%Y-%m-%d_%H:%M:%S" digunakan untuk menghasilkan string yang mencakup tahun (Y), bulan (m), tanggal (d), jam (H), menit (M), dan detik (S), yang akan digunakan sebagai nama file gambar.
5. `snprintf(image_name, sizeof(image_name), "%s/%s_%d.jpg", folder_name, timestamp, count);`: Di sini, kita menggunakan `snprintf` untuk menggabungkan folder_name, timestamp, dan nomor urutan gambar (count) menjadi nama file lengkap. Hasilnya adalah string seperti "folder_name/2023-10-13_00:00:00_1.jpg".
6. `snprintf(image_url, sizeof(image_url), "https://source.unsplash.com/%dx%d/?random", IMAGE_WIDTH_HEIGHT, IMAGE_WIDTH_HEIGHT);`: Ini adalah URL gambar yang akan diunduh. URL ini didapat dengan menggabungkan `IMAGE_WIDTH_HEIGHT` (lebar dan tinggi gambar) ke dalam URL dengan format yang sesuai.

```
 char *wget_args[] = { "wget", "-q", "-O", image_name, image_url, NULL };
   pid_t wget_pid = fork();
   if (wget_pid == 0) {
       if (execvp("wget", wget_args) == -1) {
           perror("execvp");
           exit(1);
       }
   } else if (wget_pid < 0) {
       perror("fork");
       exit(1);
   }
   int wget_status;
   waitpid(wget_pid, &wget_status, 0);
   if (WEXITSTATUS(wget_status) != 0) {
       // Handle the error here
       printf("Error: Image not found (404)\n");
       exit(1);
   }
}
```
1. `char *wget_args[] = { "wget", "-q", "-O", image_name, image_url, NULL };`: Di sini kita mendefinisikan sebuah array string `wget_args` yang akan digunakan sebagai argumen untuk menjalankan perintah `wget`. Argumen-argumen ini termasuk:
- `"wget"`: Nama perintah yang akan dieksekusi (perintah wget).
- `"-q"`: Argumen untuk menjalankan `wget` dalam mode diam (quiet mode) sehingga tidak ada output yang ditampilkan saat unduhan.
- `"-O"`: Argumen untuk menentukan nama file output (dalam hal ini, `image_name`) yang digunakan untuk menyimpan hasil unduhan.
- `image_name`: Nama file output yang telah dibentuk dengan timestamp dan nomor urutan gambar.
- `image_url`: URL dari gambar yang akan diunduh.
- `NULL`: Penanda akhir dari array argumen.
2. `pid_t wget_pid = fork();`: Di sini kita membuat proses anak dengan `fork()`. Proses anak akan digunakan untuk menjalankan perintah `wget` secara terpisah dari program utama.
3. `if (wget_pid == 0) { ... }`: Ini adalah bagian kode yang dijalankan oleh proses anak (child process). Di dalamnya, kita menggunakan `execvp` untuk menjalankan perintah `wget` dengan argumen-argumen yang telah didefinisikan dalam `wget_args`. Proses anak akan menjadi proses `wget`.
4. `else if (wget_pid < 0) { ... }`: Jika `fork()` gagal (mengembalikan nilai negatif), maka akan muncul pesan kesalahan dan program akan keluar dengan status error.
5. `int wget_status; waitpid(wget_pid, &wget_status, 0);`: Di sini, proses induk (parent process) menunggu proses anak (proses `wget`) selesai. Fungsi `waitpid` digunakan untuk menunggu hingga proses anak selesai.
6. `if (WEXITSTATUS(wget_status) != 0) { ... }`: Setelah proses anak selesai, kita memeriksa status keluarannya menggunakan `WEXITSTATUS`. Jika status keluaran bukan 0, ini menunjukkan bahwa perintah `wget` mungkin gagal. Dalam kasus ini, pesan kesalahan "Error: Image not found (404)" akan ditampilkan dan program akan keluar dengan status error.

```
void zipAndDeleteFolder(char *folder_name) {
   char zip_command[] = "zip";
   char zip_args[] = "-rq";
   char zip_file[100];
   snprintf(zip_file, sizeof(zip_file), "%s.zip", folder_name);

   pid_t zip_pid = fork();
   if (zip_pid == 0) {
       if (execlp(zip_command, zip_command, zip_args, zip_file, ".", NULL) == -1) {
           perror("execlp");
           exit(1);
       }
   } else if (zip_pid < 0) {
       perror("fork");
       exit(1);
   }
    int zip_status;
   waitpid(zip_pid, &zip_status, 0);
   if (WEXITSTATUS(zip_status) != 0) {
       perror("zip");
       exit(1);

```
1. `char zip_command[] = "zip";`: Di sini kita mendefinisikan perintah yang akan digunakan untuk mengompres folder, yaitu "zip". `char zip_args[] = "-rq";`: Ini adalah argumen yang akan digunakan untuk perintah "zip".
- `-r`: Memungkinkan pengarsipan berlangsung secara rekursif, yang berarti akan mengarsipkan semua file dan folder di dalam folder yang ditentukan.
- `-q`: Berfungsi dalam mode "quiet" atau diam, yang berarti perintah "zip" akan berjalan tanpa menampilkan output terperinci.
2. `char zip_file[100]; snprintf(zip_file, sizeof(zip_file), "%s.zip", folder_name);`: membuat variabel `zip_file` yang akan menyimpan nama file hasil pengarsipan (dalam format `.zip`). Nama file ini dibentuk dengan menambahkan ekstensi `.zip` ke nama folder yang akan diarsipkan.
3. `pid_t zip_pid = fork();`: Seperti yang telah kita lihat sebelumnya, di sini kita membuat proses anak dengan `fork()`. Proses anak ini akan digunakan untuk menjalankan perintah "zip" untuk mengompres folder.
4. `if (zip_pid == 0) { ... }`: Ini adalah kode yang dijalankan oleh proses anak (child process). Di dalamnya, kita menggunakan `execlp` untuk menjalankan perintah "zip" dengan argumen-argumen yang telah didefinisikan (termasuk nama file output, folder yang akan diarsipkan, dan argumen `-rq`). `else if (zip_pid < 0) { ... }`, Jika `fork()` gagal (mengembalikan nilai negatif), maka akan muncul pesan kesalahan dan program akan keluar dengan status error.
5. `int zip_status;`: Ini adalah variabel yang akan digunakan untuk menyimpan status keluaran dari proses "zip". Status ini mengindikasikan apakah perintah "zip" telah berhasil atau gagal.
6.  `waitpid(zip_pid, &zip_status, 0);`: Fungsi `waitpid` digunakan untuk menunggu proses dengan ID yang sesuai (`zip_pid` dalam hal ini) untuk selesai. Proses ini akan tetap dalam status "waiting" (tertunda) sampai proses anak selesai.
7. `if (WEXITSTATUS(zip_status) != 0) { ... }`: Setelah `waitpid` mengembalikan hasil, kita menggunakan fungsi `WEXITSTATUS` untuk mendapatkan status keluaran dari proses anak tersebut. Status ini akan menjadi nol jika perintah "zip" berhasil, dan bukan nol jika ada masalah atau perintah "zip" gagal.
8. `perror("zip");`: Jika status keluaran tidak nol (artinya ada masalah), maka `perror` akan mencetak pesan kesalahan yang sesuai dengan kode kesalahan. Dalam hal ini, jika "zip" gagal, akan mencetak pesan kesalahan terkait "zip". `exit(1);`: Jika ada kesalahan dalam menjalankan perintah "zip," maka program akan keluar dengan status kesalahan (1), dan ini mengindikasikan bahwa ada masalah dalam menjalankan proses tersebut.

```
 char remove_command[100];
   snprintf(remove_command, sizeof(remove_command), "rm -r %s", folder_name);
   pid_t remove_pid = fork();
   if (remove_pid == 0) {
       if (execlp("rm", "rm", "-r", folder_name, NULL) == -1) {
           perror("execlp");
           exit(1);
       }
   } else if (remove_pid < 0) {
       perror("fork");
       exit(1);
   }
    int remove_status;
   waitpid(remove_pid, &remove_status, 0);
   if (WEXITSTATUS(remove_status) != 0) {
       perror("rm");
       exit(1);
   }

   printf("Zipped and deleted folder: %s\n", folder_name);
}
```
1. `char remove_command[100];`: Ini adalah array karakter yang digunakan untuk menyimpan perintah penghapusan (dalam hal ini `rm -r`) beserta argumennya (`folder_name`) yang akan dieksekusi.
2. `snprintf(remove_command, sizeof(remove_command), "rm -r %s", folder_name);`: Fungsi `snprintf` digunakan untuk memformat string perintah penghapusan. Formatnya adalah `rm -r` diikuti oleh nama folder yang akan dihapus (`folder_name`).
3. `pid_t remove_pid = fork();`: Proses fork digunakan untuk membuat proses anak yang akan menjalankan perintah penghapusan folder. Variabel `remove_pid` akan menyimpan ID proses anak yang baru dibuat. `if (remove_pid == 0) { ... }`: Bagian ini akan dieksekusi oleh proses anak. Proses anak akan mencoba menjalankan perintah penghapusan folder menggunakan fungsi `execlp`. Jika eksekusi berhasil, proses anak akan segera keluar. `else if (remove_pid < 0) { ... }`: Bagian ini akan dieksekusi jika proses fork gagal. Ini bisa terjadi jika tidak cukup sumber daya sistem untuk membuat proses baru.
4. `perror("fork");`: Jika ada kesalahan saat membuat proses anak, pesan kesalahan akan dicetak menggunakan `perror`. `exit(1);`: Jika ada masalah dalam membuat proses anak atau menjalankan perintah penghapusan folder, program akan keluar dengan status kesalahan (1).
5. `int remove_status;`: Ini adalah variabel yang akan digunakan untuk menyimpan status keluaran dari proses anak yang menghapus folder.
6. `waitpid(remove_pid, &remove_status, 0);`: Fungsi `waitpid` digunakan untuk menunggu proses anak dengan ID `remove_pid` selesai. Fungsi ini akan mengisi variabel `remove_status` dengan status keluaran dari proses anak tersebut. Parameter `0` menunggu proses anak selesai tanpa memblokir proses ini.
7. `if (WEXITSTATUS(remove_status) != 0) { ... }`: Ini adalah bagian yang mengecek apakah status keluaran dari proses anak (penghapusan folder) sama dengan 0. Jika tidak, itu menunjukkan bahwa ada kesalahan dalam proses penghapusan.
8. `perror("rm");`: Jika ada kesalahan dalam penghapusan folder, pesan kesalahan akan dicetak dengan menggunakan `perror`. `exit(1);`: Jika terjadi kesalahan dalam penghapusan folder, program akan keluar dengan status kesalahan (1).
9. `printf("Zipped and deleted folder: %s\n", folder_name);`: Jika penghapusan folder berhasil tanpa kesalahan, maka program akan mencetak pesan sukses dengan nama folder yang telah di-zip dan dihapus.

```
void handleSIGTERM(int signum) {
   printf("Received SIGTERM signal. Terminating program...\n");
   exit(0);
}
```
1. `void handleSIGTERM(int signum) { ... }`: Ini adalah deklarasi fungsi yang akan dijalankan ketika sinyal SIGTERM diterima. Fungsi ini mengambil satu argumen `signum`, yang merupakan nomor sinyal (dalam hal ini, SIGTERM).
2. `printf("Received SIGTERM signal. Terminating program...\n");`: Ketika program menerima sinyal SIGTERM, pesan ini akan dicetak ke layar untuk memberi tahu pengguna bahwa program sedang berhenti karena menerima sinyal tersebut.
3. `exit(0);`: Ini adalah perintah yang digunakan untuk mengakhiri program dengan kode keluaran 0. Kode keluaran 0 menandakan bahwa program berhenti dengan baik dan tanpa kesalahan.

```
void createKiller(char *program_name) {
   char killer_code[] = "#include <stdio.h>\n"
                       "#include <stdlib.h>\n"
                       "#include <signal.h>\n"
                       "#include <unistd.h>\n"
                       "\n"
                       "void handleSIGTERM(int signum) {\n"
                       "    printf(\"Received SIGTERM signal. Terminating program...\\n\");\n"
                       "    kill(getppid(), SIGTERM); // Mengirim SIGTERM ke parent process (program utama)\n"
                       "    remove(\"killer\"); // Menghapus file killer setelah digunakan\n"
                       "}\n"
                       "\n"
                       "int main() {\n"
                       "    signal(SIGTERM, handleSIGTERM); // Menangani sinyal SIGTERM\n"
                       "    printf(\"Killer program started. Waiting for instructions...\\n\");\n"
                       "    while (1) {\n"
                       "        sleep(1); // Program killer berjalan dan menunggu instruksi\n"
                       "    }\n"
                       "    return 0;\n"
                       "}\n";
```
1. `char killer_code[] = "...";`: Ini adalah string yang berisi kode sumber program "killer." Kode ini akan digunakan untuk membuat file sumber program "killer."
2. Di dalam kode sumber program "killer," terdapat dua fungsi utama:
- `void handleSIGTERM(int signum) { ... }`: Ini adalah fungsi yang akan dijalankan ketika program "killer" menerima sinyal SIGTERM. Ketika sinyal SIGTERM diterima, fungsi ini mencetak pesan, kemudian mengirim sinyal SIGTERM ke parent process (program utama) dengan menggunakan `kill(getppid(), SIGTERM);`. Setelah itu, program "killer" akan menghapus dirinya sendiri dengan `remove("killer");`.
- `int main() { ... }`: Ini adalah fungsi `main` dari program "killer." Pada fungsi ini, program "killer" akan menangani sinyal SIGTERM dengan memanggil `signal(SIGTERM, handleSIGTERM);`, kemudian mencetak pesan bahwa program "killer" telah dimulai dan sedang menunggu instruksi. Selanjutnya, program "killer" akan berjalan dalam loop tanpa henti (`while (1) { ... }`) dan menunggu sinyal SIGTERM untuk dihandle oleh fungsi `handleSIGTERM`.
Setelah kode sumber program "killer" dibuat, langkah selanjutnya adalah menyimpan kode tersebut ke dalam file sumber program "killer.c," mengompilasi file tersebut menjadi executable (program "killer"), dan program "killer" ini akan siap untuk digunakan untuk menghentikan program utama dengan mengirim sinyal SIGTERM.

```
 FILE *killer_file = fopen("killer.c", "w");
   if (killer_file == NULL) {
       perror("fopen");
       exit(1);
   }
   fprintf(killer_file, "%s", killer_code);
   fclose(killer_file);

   // Kompilasi program "killer" menjadi executable
   pid_t compile_pid = fork();
   if (compile_pid == 0) {
       if (execlp("gcc", "gcc", "-o", program_name, "killer.c", NULL) == -1) {
           perror("execlp");
           exit(1);
       }
   } else if (compile_pid < 0) {
       perror("fork");
       exit(1);
   }
    int compile_status;
   waitpid(compile_pid, &compile_status, 0);
   if (WEXITSTATUS(compile_status) != 0) {
       perror("gcc");
       exit(1);
   }

   printf("Program \"%s\" created successfully.\n", program_name);
}


```
1. `FILE *killer_file = fopen("killer.c", "w");`: Pertama, sebuah file dengan nama "killer.c" dibuat dalam mode penulisan (`"w"`). Fungsi `fopen` ini mengembalikan pointer ke file yang baru dibuat atau mengembalikan `NULL` jika terdapat kesalahan.
2. `if (killer_file == NULL) { ... }`: Dilakukan pemeriksaan apakah pembukaan file "killer.c" berhasil. Jika ada kesalahan dalam pembuatan file, maka program akan keluar dengan pesan kesalahan yang dihasilkan oleh `perror("fopen")` dan keluar dengan status kesalahan (`exit(1)`).
3. `fprintf(killer_file, "%s", killer_code);`: Kode program "killer" yang telah disimpan dalam string `killer_code` dicetak ke dalam file "killer.c" menggunakan fungsi `fprintf`. Hal ini akan menyimpan kode sumber program "killer" ke dalam file. `fclose(killer_file);`: Setelah kode program "killer" berhasil disimpan dalam file, file tersebut ditutup dengan menggunakan `fclose`.
4. Selanjutnya, program "killer" akan dikompilasi menjadi executable dengan langkah berikut:
- `pid_t compile_pid = fork();`: Fork pertama dilakukan untuk menciptakan sebuah child process yang akan menjalankan perintah kompilasi.
- `if (compile_pid == 0) { ... }`: Dalam child process, perintah `execlp` digunakan untuk menjalankan kompilasi program "killer" dengan menggunakan perintah `gcc -o <nama_program> killer.c`. Ini akan menghasilkan executable dengan nama yang telah ditentukan dalam variabel `program_name`.
- `else if (compile_pid < 0) { ... }`: Jika terdapat kesalahan dalam pembuatan child process (fork gagal), maka program akan keluar dengan pesan kesalahan.
5. `int compile_status;`: Ini adalah variabel yang akan digunakan untuk menyimpan status keluaran (exit status) dari child process yang telah selesai dieksekusi. `waitpid(compile_pid, &compile_status, 0);`: Fungsi `waitpid` digunakan untuk menunggu child process dengan PID (Process ID) yang sesuai (dalam hal ini, PID dari proses kompilasi program "killer"). Status keluaran dari child process akan disimpan dalam variabel `compile_status`.
6. `if (WEXITSTATUS(compile_status) != 0) { ... }`: Setelah child process selesai dieksekusi, program memeriksa status keluaran tersebut dengan fungsi `WEXITSTATUS`. Jika status keluaran bukan 0, ini mengindikasikan bahwa ada kesalahan dalam kompilasi program "killer". `perror("gcc");`: Jika terdapat kesalahan dalam kompilasi, pesan kesalahan akan dicetak menggunakan fungsi `perror` dengan argumen "gcc". Ini akan memberikan informasi lebih lanjut tentang kesalahan yang terjadi selama kompilasi. `exit(1);`: Akhirnya, program akan keluar dengan status kesalahan (`1`) jika terdapat masalah dalam kompilasi program "killer".

```
void daemonize() {
   // Lakukan fork pertama
   pid_t pid = fork();
   if (pid < 0) {
       perror("fork");
       exit(1);
   }
   if (pid > 0) {
       exit(0); // Keluar dari parent process
   }

   // Lakukan fork kedua untuk menjadi sesi pemimpin (session leader)
   pid = fork();
   if (pid < 0) {
       perror("fork");
       exit(1);
   }
   if (pid > 0) {
       exit(0); // Keluar dari parent process
   }

   // Mengubah direktori kerja ke direktori root ("/")
   chdir("/");

   // Menutup file descriptor standar
   close(STDIN_FILENO);
   close(STDOUT_FILENO);
   close(STDERR_FILENO);
}
```
1. `pid_t pid = fork();`: Fungsi `fork` digunakan untuk menciptakan child process dari parent process yang memanggilnya. Dalam langkah ini, program mencoba untuk melakukan fork pertama. `if (pid < 0) { perror("fork"); exit(1); }`: Program memeriksa hasil dari fork pertama. Jika fork gagal (mengembalikan nilai negatif), maka `perror` digunakan untuk mencetak pesan kesalahan yang terkait dengan fork, dan program keluar dengan status kesalahan (`1`). `if (pid > 0) { exit(0); }`: Ini adalah bagian dari parent process. Jika fork pertama berhasil, parent process akan keluar dengan status `0`, dan hanya child process yang akan melanjutkan eksekusi.
2. `pid = fork();`: Setelah fork pertama, program melakukan fork kedua untuk menciptakan child process baru. `if (pid < 0) { perror("fork"); exit(1); }`: Program memeriksa hasil dari fork kedua. Jika fork gagal, pesan kesalahan akan dicetak, dan program keluar dengan status kesalahan (`1`). `if (pid > 0) { exit(0); }`: Ini adalah bagian dari parent process kedua. Jika fork kedua berhasil, parent process kedua akan keluar dengan status `0`, dan hanya child process kedua yang akan melanjutkan eksekusi.
3. `chdir("/");`: Fungsi ini digunakan untuk mengubah direktori kerja saat ini ke direktori root ("/"). Ini adalah langkah penting untuk menghindari masalah saat direktori kerja saat ini terkunci atau tidak dapat dihapus.
4. `close(STDIN_FILENO)`, `close(STDOUT_FILENO)`, `close(STDERR_FILENO)`: Tiga file descriptor standar, yaitu file descriptor untuk stdin (0), stdout (1), dan stderr (2), ditutup. Ini dilakukan untuk memastikan bahwa program tidak akan mencoba membaca atau menulis ke file descriptor standar setelah menjadi daemon. Hal ini juga membantu dalam melepaskan koneksi ke terminal, sehingga program dapat berjalan secara mandiri dan tidak dipengaruhi oleh terminal.

```
int main(int argc, char *argv[]) {
   if (argc != 2 || (strcmp(argv[1], "-a") != 0 && strcmp(argv[1], "-b") != 0)) {
       printf("Gunakan argumen -a untuk MODE_A atau -b untuk MODE_B.\n");
       exit(1);
   }
   int mode_a = (strcmp(argv[1], "-a") == 0);

   // Menjadi daemon
   daemonize();

   // Mengehandle sinyal SIGTERM (seperti sebelumnya)
   signal(SIGTERM, handleSIGTERM);

   // Buat program "killer" seperti sebelumnya
   createKiller("killer");

   int folder_count = 0;
   int image_count = 0;
while (1) {
       char folder_name[50];
       time_t current_time;
       struct tm *time_info;
       char timestamp[20];

       time(&current_time);
       time_info = localtime(&current_time);

       strftime(timestamp, sizeof(timestamp), "%Y-%m-%d_%H:%M:%S", time_info);
       snprintf(folder_name, sizeof(folder_name), "%s", timestamp);

       createFolder(folder_name);

       for (int i = 0; i < MAX_IMAGES; i++) {
           downloadImage(folder_name, image_count);
           image_count++;
           sleep(INTERVAL_IMAGE);
       }

       folder_count++;
       image_count = 0;
        if (folder_count >= MAX_FOLDERS) {
           if (!mode_a) {
               zipAllFolders(); // Panggil fungsi zipAllFolders untuk mengompres semua folder
           }
           folder_count = 0;

           if (mode_a) {
               printf("MODE_A: Program utama dihentikan karena program killer dijalankan.\n");
               break;
           }
       } else {
           zipAndDeleteFolder(folder_name); // Di-zip segera setelah folder selesai diisi gambar
       }
       sleep(INTERVAL_FOLDER);
   }
   return 0;
}
```
1. `if (argc != 2 || (strcmp(argv[1], "-a") != 0 && strcmp(argv[1], "-b") != 0))`: Program memeriksa apakah jumlah argumen yang diberikan pada baris perintah adalah tepat satu (yaitu argumen mode `-a` atau `-b`). Jika jumlah argumen tidak sesuai atau argumen yang diberikan bukan `-a` atau `-b`, maka program akan mencetak pesan kesalahan dan keluar dengan status kesalahan (`1`). `int mode_a = (strcmp(argv[1], "-a") == 0)`: Program menentukan mode berdasarkan argumen yang diberikan. Jika argumen adalah `-a`, variabel `mode_a` akan diatur ke `1`, yang menunjukkan mode A. Jika argumen adalah `-b`, `mode_a` akan diatur ke `0`, yang menunjukkan mode B.
2. `daemonize();`: Program menjalankan fungsi `daemonize()` untuk menjadikan dirinya sebagai daemon, seperti yang dijelaskan sebelumnya.
3. `signal(SIGTERM, handleSIGTERM);`: Program mengatur penanganan sinyal SIGTERM dengan menggunakan fungsi `signal`. Jika program menerima sinyal SIGTERM, ia akan menjalankan fungsi `handleSIGTERM` yang telah didefinisikan sebelumnya. `createKiller("killer");`: Program membuat program "killer" seperti yang dijelaskan sebelumnya. Fungsi ini menciptakan file "killer.c" dan mengompilasinya menjadi executable "killer". Setelah itu, program "killer" dapat digunakan untuk menghentikan program utama.
4. `int folder_count = 0;` dan `int image_count = 0;`: Variabel `folder_count` dan `image_count` diinisialisasi. Mereka digunakan untuk melacak berapa banyak folder yang telah dibuat dan berapa banyak gambar yang telah diunduh dalam mode program.
5. `char folder_name[50];`: Variabel `folder_name` digunakan untuk menyimpan nama folder yang akan dibuat. Ini akan mengandung timestamp sesuai dengan format tahun-bulan-tanggal_jam:menit:detik.
6. `time_t current_time; struct tm *time_info; char timestamp[20];`: Variabel `current_time` digunakan untuk menyimpan waktu saat ini. Variabel `time_info` adalah pointer ke struktur `tm` yang digunakan untuk menyimpan informasi waktu terurai seperti tahun, bulan, dan sebagainya. Variabel `timestamp` digunakan untuk menyimpan timestamp dalam format yang sesuai. `time(&current_time); time_info = localtime(&current_time);`: Program mendapatkan waktu saat ini menggunakan `time(&current_time)` dan kemudian mengurai informasi waktu tersebut ke dalam `time_info` dengan menggunakan `localtime(&current_time)`. `strftime(timestamp, sizeof(timestamp), "%Y-%m-%d_%H:%M:%S", time_info);`: Variabel `timestamp` diisi dengan timestamp yang dibentuk dari informasi waktu yang sudah terurai. Format timestamp adalah "tahun-bulan-tanggal_jam:menit:detik", sesuai dengan yang telah ditentukan dalam kode.
7. `snprintf(folder_name, sizeof(folder_name), "%s", timestamp);`: Variabel `folder_name` diisi dengan timestamp yang sudah dibuat. Ini akan digunakan sebagai nama folder yang akan dibuat. `createFolder(folder_name);`: Fungsi `createFolder()` dipanggil untuk membuat folder dengan nama yang sesuai timestamp yang telah dibuat.
8. `for (int i = 0; i < MAX_IMAGES; i++) { ... }`: Program akan melakukan loop ini untuk mengunduh gambar sebanyak `MAX_IMAGES` (nilai yang telah ditentukan sebelumnya). Dalam loop ini, fungsi `downloadImage()` dipanggil untuk mengunduh gambar ke dalam folder yang sesuai. `image_count++;`: Variabel `image_count` akan diinkrementasi setelah mengunduh setiap gambar untuk melacak berapa banyak gambar yang sudah diunduh dalam folder saat ini.
9. `sleep(INTERVAL_IMAGE);`: Program akan tidur (sleep) selama jeda waktu yang ditentukan oleh `INTERVAL_IMAGE` sebelum mengunduh gambar berikutnya. Ini membantu mengendalikan seberapa cepat gambar diunduh.
10. `folder_count++;`: Variabel `folder_count` akan diinkrementasi setelah folder saat ini selesai diisi dengan gambar-gambar. Ini digunakan untuk melacak berapa banyak folder yang telah dibuat. `image_count = 0;`: Setelah folder saat ini selesai diisi dengan gambar, variabel `image_count` diatur kembali ke 0 untuk menghitung gambar dalam folder berikutnya. Ini membantu untuk menjaga penghitungan gambar dalam setiap folder terpisah.
11. `if (folder_count >= MAX_FOLDERS) { ... }`: Kondisi ini memeriksa apakah jumlah folder yang telah dibuat (`folder_count`) sudah mencapai atau melebihi nilai maksimum yang ditentukan oleh `MAX_FOLDERS`. Ini adalah salah satu dari dua kondisi berhenti yang dapat menghentikan program tergantung pada mode yang dipilih (mode -a atau -b).
12. `if (!mode_a) { ... }`: Jika program berjalan dalam mode -b (MODE_B), maka kode ini akan dieksekusi. Dalam mode -b, program akan memanggil fungsi `zipAllFolders()` untuk mengompres semua folder yang sudah ada. Dalam konteks ini, `zipAllFolders()` bertugas untuk mengompres semua folder yang sudah dibuat dan kemudian membentuk satu file ZIP (`all_folders.zip`) yang berisi semua folder tersebut.
13. `folder_count = 0;`: Setelah semua folder selesai di-zip atau setelah satu folder dihapus (mode -b), variabel `folder_count` diatur kembali ke 0 untuk memulai proses pembuatan folder yang baru.
14. `if (mode_a) { ... }`: Jika program berjalan dalam mode -a (MODE_A), maka kode ini akan dieksekusi. Dalam mode -a, program mencetak pesan bahwa program utama dihentikan karena program killer dijalankan, dan kemudian program keluar dari loop dengan pernyataan `break`. Ini berarti program utama akan berhenti setelah semua folder saat ini selesai diisi gambar.`else { ... }`: Dalam mode -a, program akan mencapai kode ini. Dalam konteks ini, program akan memanggil fungsi `zipAndDeleteFolder(folder_name)` untuk mengompres folder saat ini (yang sudah selesai diisi dengan gambar) dan kemudian menghapus folder yang di-zip tersebut.
15. `sleep(INTERVAL_FOLDER);`: Setelah menyelesaikan operasi terkait folder saat ini, program akan tidur selama jeda waktu yang ditentukan oleh `INTERVAL_FOLDER` sebelum melanjutkan membuat folder baru atau mengompres folder sesuai kondisi. `return 0;`: Ini adalah akhir dari `main` function. Setelah loop selesai dan program selesai berjalan, program akan mengembalikan status keluar 0, menandakan bahwa program selesai berjalan tanpa kesalahan.

# Output 

Berikut bentuk dari directory ketika program sudah dijalankan : 
![tree firectory](https://github.com/J0see1/BMS-KPP/assets/134209563/8e514cc1-546b-4515-a893-eada2c56d929)

Berikut ketika proses A dijalankan :
![folder a zip delete mode a](https://github.com/J0see1/BMS-KPP/assets/134209563/0de70134-1a8b-4f32-8a9c-0465d96951b7)

Berikut ketika proses B dijalankan : 
![folder b zip delete mode b](https://github.com/J0see1/BMS-KPP/assets/134209563/e5686ecb-e951-4aa4-9f2b-d82c0d2a4fa6)

Berikut proses killer mode a :
![killer mode a](https://github.com/J0see1/BMS-KPP/assets/134209563/ee9a7632-ad86-458d-ae69-7aa6d9978033)

# Soal 4
Choco adalah seorang ahli pertahanan siber yang tidak suka memakai ChatGPT dalam menyelesaikan masalah. Dia selalu siap melindungi data dan informasi dari ancaman dunia maya. Namun, kali ini, dia membutuhkan bantuan Anda untuk meningkatkan kinerja antivirus yang telah dia buat sebelumnya.

Bantu Choco dalam mengoptimalkan program antivirus bernama antivirus.c. Program ini seharusnya dapat memeriksa file di folder sisop_infected, dan jika file tersebut diidentifikasi sebagai virus berdasarkan ekstensinya, program harus memindahkannya ke folder quarantine. list dari format ekstensi/tipe file nya bisa didownload di Link Ini , proses mendownload tidak boleh menggunakan system()
Daftar ekstensi file yang dianggap virus tersimpan dalam file extensions.csv.
Ada kejutan di dalam file extensions.csv. Hanya 8 baris pertama yang tidak dienkripsi. Baris-baris setelahnya perlu Anda dekripsi menggunakan algoritma rot13 untuk mengetahui ekstensi virus lainnya.Setiap kali program mendeteksi file virus, catatlah informasi tersebut di virus.log. Format log harus sesuai dengan:
[nama_user][Dd-Mm-Yy:Hh-Mm-Ss] - {nama file yang terinfeksi} - {tindakan yang diambil}

Contoh:  [sisopUser][29-09-23:08-59-01] - test.locked - Moved to quarantine

*nama_user: adalah username dari user yang menambahkan file ter-infected

Dunia siber tidak pernah tidur, dan demikian juga virus. Choco memerlukan antivirus yang terus berjalan di latar belakang tanpa harus dia intervensi. Dengan menjalankan program ini sebagai Latar belakang, program akan secara otomatis memeriksa folder sisop_infected setiap detik.
Choco juga membutuhkan level-level keamanan antivrus jadi dia membuat 3 level yaitu low, medium ,hard. Argumen tersebut di pakai saat menjalankan antivirus.

    Low: Hanya me-log file yg terdeteksi
    Medium: log dan memindahkan file yang terdeteksi
    Hard: log dan menghapus file yang terdeteksi

ex: ./antivirus -p low

Kadang-kadang, Choco mungkin perlu mengganti level keamanan dari antivirus tanpa harus menghentikannya. Integrasikan kemampuan untuk mengganti level keamanan antivirus dengan mengirim sinyal ke daemon. Misalnya, menggunakan SIGUSR1 untuk mode "low", SIGUSR2 untuk "medium", dan SIGRTMIN untuk mode "hard".

Contoh:

kill -SIGUSR1 <pid_program>



Meskipun penting untuk menjalankan antivirus, ada saat-saat Choco mungkin perlu menonaktifkannya sementara. Bantu dia dengan menyediakan fitur untuk mematikan antivirus dengan cara yang aman dan efisien.

# Analisis
Berikut kode program secara full : 
```C
#include <stdio.h>           
#include <stdlib.h>          
#include <string.h>          
#include <signal.h>          
#include <unistd.h>          
#include <sys/stat.h>            
#include <dirent.h> 
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>


int status;

void decryptExtensions() {
    FILE *extensions_file = fopen("extensions.csv", "r");
    FILE *decrypted_file = fopen("decrypted_extensions.csv", "w");

    if (!extensions_file || !decrypted_file) {
        perror("Error opening files");
        exit(EXIT_FAILURE);
    }

    char line[256];
    while (fgets(line, sizeof(line), extensions_file)) {
        if (line[0] == '\n') {
            break;
        }

        char *ext = strchr(line, ',');
        if (ext) {
            ext++; // Move to the start of the extension
            for (int i = 0; ext[i] != '\0'; i++) {
                if ((ext[i] >= 'A' && ext[i] <= 'M') || (ext[i] >= 'a' && ext[i] <= 'm')) {
                    ext[i] += 13;
                } else if ((ext[i] >= 'N' && ext[i] <= 'Z') || (ext[i] >= 'n' && ext[i] <= 'z')) {
                    ext[i] -= 13;
                }
            }
        }
        
        fprintf(decrypted_file, "%s", line);
    }

    fclose(extensions_file);
    fclose(decrypted_file);
    rename("decrypted_extensions.csv", "extensions.csv");
}

void makeDirectory() {
    pid_t mkdirSisop, mkdirQuarantine;

    mkdirSisop = fork();

    if (mkdirSisop == 0) {
        int create = mkdir("sisop_infected", 0777); 
        if (create != 0) {
            perror("Error creating directory");
        } else {
            printf("Directory sisop_infected created successfully.\n");
        }
        exit(EXIT_SUCCESS);
    } else if (mkdirSisop > 0) {
        mkdirQuarantine = fork();
        
        if (mkdirQuarantine == 0) {
            int create = mkdir("quarantine", 0777); 
            if (create != 0) {
                perror("Error creating directory");
            } else {
                printf("Directory quarantine created successfully.\n");
            }
            exit(EXIT_SUCCESS);
        } else if (mkdirQuarantine < 0) {
            perror("Error forking child process 2");
        }
    } else {
        perror("Error forking child process 1");
    }
}

void downloadFile() {
    pid_t wget = fork();
    if (wget == 0){
        execl("/usr/bin/wget", "wget", "-O", "extensions.csv", "https://drive.google.com/u/0/uc?id=1gIhwR7JLnH5ZBljmjkECzi8tIlW8On_5&export=download", NULL );
        exit(EXIT_SUCCESS);
    } else if (wget < 0) {
        perror("Fork failed");
        exit(EXIT_FAILURE);
    } else {
        int status; 
        waitpid(wget, &status, 0);
    }
}

void move_file(const char *source_path, const char *destination_dir) {
    pid_t pid = fork();
    if (pid == 0) {
        execlp("mv", "mv", source_path, destination_dir, NULL);
        perror("Error executing execlp");
        exit(EXIT_FAILURE); // Jika execlp gagal
    } else if (pid > 0) {
        int status;
        waitpid(pid, &status, 0);
    } else {
        perror("Error forking child process");
    }
}

void logVirusInfo(char *username, char *filename, char *action) {
    pid_t pid = fork();
    if (pid == 0) {
        char timestamp[20];
        time_t t;
        struct tm *tm_info;

        time(&t);
        tm_info = localtime(&t);

        strftime(timestamp, 20, "%d-%m-%y:%H-%M-%S", tm_info);

        char log_entry[256];
        snprintf(log_entry, sizeof(log_entry), "[%s][%s] - %s - %s", username, timestamp, filename, action);

        FILE *log_file = fopen("virus.log", "a");
        if (log_file != NULL) {
            fprintf(log_file, "%s\n", log_entry);
            fclose(log_file);
            exit(EXIT_SUCCESS);
        } else {
            perror("Error opening virus.log");
            exit(EXIT_FAILURE);
        }
    } else if (pid > 0) {
        int status;
        waitpid(pid, &status, 0);
    } else {
        perror("Error forking child process");
    }
}




void matchFiles() {
    char line[256];
    FILE *extensions_file = fopen("extensions.csv", "r");
    if (!extensions_file) {
        perror("Error opening extensions.csv");
    }

    while (fgets(line, sizeof(line), extensions_file)) {
        line[strcspn(line, "\n")] = 0;  

        DIR *dir;
        struct dirent *entry;

        dir = opendir("sisop_infected/");
        if (!dir) {
            perror("Error opening directory sisop_infected/");
        }

        while ((entry = readdir(dir))) {
            if (entry->d_type == DT_REG) { 
                char *ext = strrchr(entry->d_name, '.');
                if (ext != NULL && strstr(line, ext) != NULL) {
                    char source_path[300]; 
                    snprintf(source_path, sizeof(source_path), "sisop_infected/%s", entry->d_name);
                    move_file(source_path, "quarantine/");

                    // Mencatat informasi di virus.log
                    logVirusInfo("sisopUser", entry->d_name, "Moved to quarantine");
                }
            }
        }

        closedir(dir);
    }

    fclose(extensions_file);
}

void dummyFiles() {
    int pid = fork();
    if (pid == 0) {
        // Child process
        system("echo hello > sisop_infected/dsakdlmsa*.RAPE");
    } else if (pid > 0) {
        // Parent process
        int status;
        waitpid(pid, &status, 0);
    }
}

    void daemonize() {
        pid_t pid, sid;
        pid = fork();

        if (pid < 0) {
            exit(EXIT_FAILURE);
        }

        if (pid > 0) {
            exit(EXIT_SUCCESS);
        }

        umask(0);
        sid = setsid();

        if (sid < 0) {
            exit(EXIT_FAILURE);
        }

        if ((chdir("/")) < 0) {
            exit(EXIT_FAILURE);
        }

        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);
    }

int main() {


    makeDirectory();
    downloadFile();
    dummyFiles();
    decryptExtensions();

while(1){
    matchFiles();
    sleep(1);
}

    return 0;
}

```

# Analisis Tiap Fungsi 

- Fungsi di bawah adalah untuk membuat directory sisop_infected dan quarantine secara bersamaan menggunakan fork().

```c
void makeDirectory() {
    pid_t mkdirSisop, mkdirQuarantine;

    mkdirSisop = fork();

    if (mkdirSisop == 0) {
        int create = mkdir("sisop_infected", 0777); 
        if (create != 0) {
            perror("Error creating directory");
        } else {
            printf("Directory sisop_infected created successfully.\n");
        }
        exit(EXIT_SUCCESS);
    } else if (mkdirSisop > 0) {
        mkdirQuarantine = fork();
        
        if (mkdirQuarantine == 0) {
            int create = mkdir("quarantine", 0777); 
            if (create != 0) {
                perror("Error creating directory");
            } else {
                printf("Directory quarantine created successfully.\n");
            }
            exit(EXIT_SUCCESS);
        } else if (mkdirQuarantine < 0) {
            perror("Error forking child process 2");
        }
    } else {
        perror("Error forking child process 1");
    }
}
```

- Fungsi di bawah adalah untuk download file extension.csv menggunakan wget.

```c
void downloadFile() {
    pid_t wget = fork();
    if (wget == 0){
        execl("/usr/bin/wget", "wget", "-O", "extensions.csv", "https://drive.google.com/u/0/uc?id=1gIhwR7JLnH5ZBljmjkECzi8tIlW8On_5&export=download", NULL );
        exit(EXIT_SUCCESS);
    } else if (wget < 0) {
        perror("Fork failed");
        exit(EXIT_FAILURE);
    } else {
        int status; 
        waitpid(wget, &status, 0);
    }
}
```

- Fungsi di bawah adalah untuk melakukan pemindahan file dari folder sisop_infected ke quarantine.

```c
void move_file(const char *source_path, const char *destination_dir) {
    pid_t pid = fork();
    if (pid == 0) {
        execlp("mv", "mv", source_path, destination_dir, NULL);
        perror("Error executing execlp");
        exit(EXIT_FAILURE); // Jika execlp gagal
    } else if (pid > 0) {
        int status;
        waitpid(pid, &status, 0);
    } else {
        perror("Error forking child process");
    }
}
```

- fungsi di bawah adalah untuk menuliskan log pemindahan file tadi ke file virus.log.

```c
void logVirusInfo(char *username, char *filename, char *action) {
    pid_t pid = fork();
    if (pid == 0) {
        char timestamp[20];
        time_t t;
        struct tm *tm_info;

        time(&t);
        tm_info = localtime(&t);

        strftime(timestamp, 20, "%d-%m-%y:%H-%M-%S", tm_info);

        char log_entry[256];
        snprintf(log_entry, sizeof(log_entry), "[%s][%s] - %s - %s", username, timestamp, filename, action);

        FILE *log_file = fopen("virus.log", "a");
        if (log_file != NULL) {
            fprintf(log_file, "%s\n", log_entry);
            fclose(log_file);
            exit(EXIT_SUCCESS);
        } else {
            perror("Error opening virus.log");
            exit(EXIT_FAILURE);
        }
    } else if (pid > 0) {
        int status;
        waitpid(pid, &status, 0);
    } else {
        perror("Error forking child process");
    }
}
```

- fungsi di bawah berguna untuk menyamakan elemen kata dalam nama file di folder sisop_infected dengan isi dari extensions.csv.

```c
void matchFiles() {
    char line[256];
    FILE *extensions_file = fopen("extensions.csv", "r");
    if (!extensions_file) {
        perror("Error opening extensions.csv");
    }

    while (fgets(line, sizeof(line), extensions_file)) {
        line[strcspn(line, "\n")] = 0;  

        DIR *dir;
        struct dirent *entry;

        dir = opendir("sisop_infected/");
        if (!dir) {
            perror("Error opening directory sisop_infected/");
        }

        while ((entry = readdir(dir))) {
            if (entry->d_type == DT_REG) { 
                char *ext = strrchr(entry->d_name, '.');
                if (ext != NULL && strstr(line, ext) != NULL) {
                    char source_path[300]; 
                    snprintf(source_path, sizeof(source_path), "sisop_infected/%s", entry->d_name);
                    move_file(source_path, "quarantine/");

                    // Mencatat informasi di virus.log
                    logVirusInfo("sisopUser", entry->d_name, "Moved to quarantine");
                }
            }
        }

        closedir(dir);
    }

    fclose(extensions_file);
}
```

- fungsi di bawah berguna untuk mendaemonisasi program .

```c
void daemonize() {
        pid_t pid, sid;
        pid = fork();

        if (pid < 0) {
            exit(EXIT_FAILURE);
        }

        if (pid > 0) {
            exit(EXIT_SUCCESS);
        }

        umask(0);
        sid = setsid();

        if (sid < 0) {
            exit(EXIT_FAILURE);
        }

        if ((chdir("/")) < 0) {
            exit(EXIT_FAILURE);
        }

        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);
    }
```

- dan berikut adalah fungsi main dari program, di mana tiap fungsi di atas tadi dipanggil dan untuk melakukan deamon digunakan while(1) dan sleep untuk memberikan delay 1 detik untuk progrm berjalan : 

 ```c
int main() {

makeDirectory();
downloadFile();
dummyFiles();
DecryptExtensions();

while(1){
    matchFiles();
    sleep(1);
}

    return 0;
}
```

# Output

- Berikut output dari program, dapat dilihat bahwa isi dari virus.log berhasil memindahkan file yang bagian belakangnya sama dengan isi extensions.csv ke folder quarantine.

![output](https://github.com/J0see1/KPP-BMS/assets/134209563/6b1901b6-97d0-46d8-a740-7e6dd95394c8)