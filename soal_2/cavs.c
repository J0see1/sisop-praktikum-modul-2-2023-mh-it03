#include <stdio.h>
#include <curl/curl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <stdint.h>
#include <dirent.h>

int status;

void makeDirectory(){
pid_t mkdir = fork();
if (mkdir == 0){
    execl("/usr/bin/mkdir", "mkdir", "-p", "players", NULL);
}
}

void downloadFile(){
    pid_t wget = fork();
    if (wget == 0){
        execl("/usr/bin/wget", "wget", "-O", "players/players.zip", "https://drive.google.com/u/0/uc?id=1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK&export=download", NULL );
    } waitpid(wget, &status,0);
}

void extractZip(){
    pid_t unzip = fork();
    if (unzip == 0) {
        execl("/usr/bin/unzip", "unzip", "players/players.zip", "-d", "players", NULL);
        perror("execl");
        exit(EXIT_FAILURE); // Tambahkan exit jika execl gagal
    }
    else if (unzip > 0) {

        waitpid(unzip, &status, 0); // Tunggu proses anak selesai
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            // Proses anak selesai dengan sukses
            remove("players/players.zip"); // Hapus berkas ZIP
        } else {
            printf("Unzip failed.\n");
        }
    }
    else {
        perror("fork");
    }
    waitpid(unzip, &status, 0);
}

void removeNonCavsPlayers() {
    pid_t remove = fork();
    if (remove == 0) {
        execlp("find", "find", "players", "-type", "f", "!", "-name", "Cavaliers*", "-delete", NULL);
        perror("execlp");
        exit(EXIT_FAILURE);
    } else if (remove > 0) {
        waitpid(remove, &status, 0);
    } else {
        perror("fork");
    }
}

void createPGFolderAndMovePlayers() {
    pid_t mkdirPG = fork(); // Membuat proses anak
    
    if (mkdirPG == 0) {
        // Proses anak
        execl("/bin/mkdir", "mkdir", "-p", "players/PG", NULL); // Memanggil mkdir melalui exec
        perror("execlp"); // Jika exec gagal
        exit(EXIT_FAILURE);
    } else if (mkdirPG > 0) {
        // Proses induk menunggu anak selesai
        int status;
        waitpid(mkdirPG, &status, 0);
        
        // Setelah folder PG dibuat, kita pindahkan pemain dengan PG di dalam namanya
        pid_t movePG = fork(); // Membuat proses anak lagi
        if (movePG == 0) {
            // Proses anak
            execl("/bin/sh", "sh", "-c", "mv players/*PG* players/PG/", NULL); // Memanggil mv melalui exec
            perror("execlp"); // Jika exec gagal
            exit(EXIT_FAILURE);
        } else if (movePG > 0) {
            // Proses induk menunggu anak selesai
            int moveStatus;
            waitpid(movePG, &moveStatus, 0);
        } else {
            perror("fork"); // Jika fork gagal
        }
    } else {
        perror("fork"); // Jika fork gagal
    }
}

void createSGFolderAndMovePlayers() {
    pid_t mkdirSG = fork(); // Membuat proses anak
    
    if (mkdirSG == 0) {
        // Proses anak
        execl("/bin/mkdir", "mkdir", "-p", "players/SG", NULL); // Memanggil mkdir melalui exec
        perror("execlp"); // Jika exec gagal
        exit(EXIT_FAILURE);
    } else if (mkdirSG > 0) {
        // Proses induk menunggu anak selesai
        int status;
        waitpid(mkdirSG, &status, 0);
        
        // Setelah folder SG dibuat, kita pindahkan pemain dengan SG di dalam namanya
        pid_t moveSG = fork(); // Membuat proses anak lagi
        if (moveSG == 0) {
            // Proses anak
            execl("/bin/sh", "sh", "-c", "mv players/*SG* players/SG/", NULL); // Memanggil mv melalui exec
            perror("execlp"); // Jika exec gagal
            exit(EXIT_FAILURE);
        } else if (moveSG > 0) {
            // Proses induk menunggu anak selesai
            int moveStatus;
            waitpid(moveSG, &moveStatus, 0);
        } else {
            perror("fork"); // Jika fork gagal
        }
    } else {
        perror("fork"); // Jika fork gagal
    }
}

void createSFFolderAndMovePlayers() {
    pid_t mkdirSF = fork(); // Membuat proses anak
    
    if (mkdirSF == 0) {
        // Proses anak
        execl("/bin/mkdir", "mkdir", "-p", "players/SF", NULL); // Memanggil mkdir melalui exec
        perror("execlp"); // Jika exec gagal
        exit(EXIT_FAILURE);
    } else if (mkdirSF > 0) {
        // Proses induk menunggu anak selesai
        int status;
        waitpid(mkdirSF, &status, 0);
        
        // Setelah folder SF dibuat, kita pindahkan pemain dengan SF di dalam namanya
        pid_t moveSF = fork(); // Membuat proses anak lagi
        if (moveSF == 0) {
            // Proses anak
            execl("/bin/sh", "sh", "-c", "mv players/*SF* players/SF/", NULL); // Memanggil mv melalui exec
            perror("execlp"); // Jika exec gagal
            exit(EXIT_FAILURE);
        } else if (moveSF > 0) {
            // Proses induk menunggu anak selesai
            int moveStatus;
            waitpid(moveSF, &moveStatus, 0);
        } else {
            perror("fork"); // Jika fork gagal
        }
    } else {
        perror("fork"); // Jika fork gagal
    }
}

void createPFFolderAndMovePlayers() {
    pid_t mkdirPF = fork(); // Membuat proses anak
    
    if (mkdirPF == 0) {
        // Proses anak
        execl("/bin/mkdir", "mkdir", "-p", "players/PF", NULL); // Memanggil mkdir melalui exec
        perror("execlp"); // Jika exec gagal
        exit(EXIT_FAILURE);
    } else if (mkdirPF > 0) {
        // Proses induk menunggu anak selesai
        int status;
        waitpid(mkdirPF, &status, 0);
        
        // Setelah folder PF dibuat, kita pindahkan pemain dengan PF di dalam namanya
        pid_t movePF = fork(); // Membuat proses anak lagi
        if (movePF == 0) {
            // Proses anak
            execl("/bin/sh", "sh", "-c", "mv players/*PF* players/PF/", NULL); // Memanggil mv melalui exec
            perror("execlp"); // Jika exec gagal
            exit(EXIT_FAILURE);
        } else if (movePF > 0) {
            // Proses induk menunggu anak selesai
            int moveStatus;
            waitpid(movePF, &moveStatus, 0);
        } else {
            perror("fork"); // Jika fork gagal
        }
    } else {
        perror("fork"); // Jika fork gagal
    }
}

void createCFolderAndMovePlayers() {
    pid_t mkdirC = fork(); // Membuat proses anak
    
    if (mkdirC == 0) {
        // Proses anak
        execl("/bin/mkdir", "mkdir", "-p", "players/C", NULL); // Memanggil mkdir melalui exec
        perror("execlp"); // Jika exec gagal
        exit(EXIT_FAILURE);
    } else if (mkdirC > 0) {
        // Proses induk menunggu anak selesai
        int status;
        waitpid(mkdirC, &status, 0);
        
        // Setelah folder C dibuat, kita pindahkan pemain dengan C di dalam namanya
        pid_t moveC = fork(); // Membuat proses anak lagi
        if (moveC == 0) {
            // Proses anak
            execl("/bin/sh", "sh", "-c", "mv players/*C* players/C/", NULL); // Memanggil mv melalui exec
            perror("execlp"); // Jika exec gagal
            exit(EXIT_FAILURE);
        } else if (moveC > 0) {
            // Proses induk menunggu anak selesai
            int moveStatus;
            waitpid(moveC, &moveStatus, 0);
        } else {
            perror("fork"); // Jika fork gagal
        }
    } else {
        perror("fork"); // Jika fork gagal
    }
}

void generateFormationFile() {
    int pg_count = 0, sg_count = 0, sf_count = 0, pf_count = 0, c_count = 0;

    // Loop through each position folder
    char positions[5][3] = {"PG", "SG", "SF", "PF", "C"};
    for (int i = 0; i < 5; i++) {
        DIR *d;
        struct dirent *dir;
        char position_folder[32];
        snprintf(position_folder, sizeof(position_folder), "players/%s", positions[i]);
        d = opendir(position_folder);
        if (d) {
            while ((dir = readdir(d)) != NULL) {
                if (strstr(dir->d_name, "Cavaliers") != NULL && strstr(dir->d_name, ".png") != NULL) {
                    if (i == 0) pg_count++;
                    else if (i == 1) sg_count++;
                    else if (i == 2) sf_count++;
                    else if (i == 3) pf_count++;
                    else if (i == 4) c_count++;
                }
            }
            closedir(d);
        }
    }

    FILE *formation_file = fopen("Formasi.txt", "w");
    if (formation_file) {
        fprintf(formation_file, "PG: %d\n", pg_count);
        fprintf(formation_file, "SG: %d\n", sg_count);
        fprintf(formation_file, "SF: %d\n", sf_count);
        fprintf(formation_file, "PF: %d\n", pf_count);
        fprintf(formation_file, "C: %d\n", c_count);
        fclose(formation_file);
    } else {
        perror("Error creating formation file");
    }
}

void createClutchFolder() {
    int create = mkdir("clutch", 0777);
    if (create != 0) {
        perror("Error creating clutch directory");
    } else {
        printf("Clutch directory created successfully.\n");
    }
}

void moveClutchPhoto() {
    char src[512];
    char dst[512];
    char *  ClutchplayerName = "Cavaliers-PG-Kyrie-Irving.png";
    snprintf(src, sizeof(src), "players/PG/%s", ClutchplayerName);
    snprintf(dst, sizeof(dst), "clutch/%s", ClutchplayerName);

    int success = rename(src, dst);
    if (success == 0) {
        printf("Moved %s to clutch folder successfully.\n", ClutchplayerName);
    } else {
        perror("Error moving clutch photo");
    }
}

void moveTheBlockPhoto() {
    char src[512];
    char dst[512];
    char *  BlockplayerName = "Cavaliers-SF-LeBron-James.png";
    snprintf(src, sizeof(src), "players/SF/%s", BlockplayerName);
    snprintf(dst, sizeof(dst), "clutch/%s", BlockplayerName);

    int success = rename(src, dst);
    if (success == 0) {
        printf("Moved %s to clutch folder successfully.\n", BlockplayerName);
    } else {
        perror("Error moving clutch photo");
    }
}

int main(){
    makeDirectory();
    downloadFile();
    extractZip();
    removeNonCavsPlayers();
    createPGFolderAndMovePlayers();
    createSGFolderAndMovePlayers();
    createSFFolderAndMovePlayers();
    createPFFolderAndMovePlayers();
    createCFolderAndMovePlayers();
    generateFormationFile();
    createClutchFolder();
    moveClutchPhoto();
    moveTheBlockPhoto();
}