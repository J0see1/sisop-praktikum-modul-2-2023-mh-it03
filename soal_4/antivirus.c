#include <stdio.h>           
#include <stdlib.h>          
#include <string.h>          
#include <signal.h>          
#include <unistd.h>          
#include <sys/stat.h>            
#include <dirent.h> 
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>


int status;

void decryptExtensions() {
    FILE *extensions_file = fopen("extensions.csv", "r");
    FILE *decrypted_file = fopen("decrypted_extensions.csv", "w");

    if (!extensions_file || !decrypted_file) {
        perror("Error opening files");
        exit(EXIT_FAILURE);
    }

    char line[256];
    while (fgets(line, sizeof(line), extensions_file)) {
        if (line[0] == '\n') {
            break;
        }

        char *ext = strchr(line, ',');
        if (ext) {
            ext++; // Move to the start of the extension
            for (int i = 0; ext[i] != '\0'; i++) {
                if ((ext[i] >= 'A' && ext[i] <= 'M') || (ext[i] >= 'a' && ext[i] <= 'm')) {
                    ext[i] += 13;
                } else if ((ext[i] >= 'N' && ext[i] <= 'Z') || (ext[i] >= 'n' && ext[i] <= 'z')) {
                    ext[i] -= 13;
                }
            }
        }
        
        fprintf(decrypted_file, "%s", line);
    }

    fclose(extensions_file);
    fclose(decrypted_file);
    rename("decrypted_extensions.csv", "extensions.csv");
}

void makeDirectory() {
    pid_t mkdirSisop, mkdirQuarantine;

    mkdirSisop = fork();

    if (mkdirSisop == 0) {
        int create = mkdir("sisop_infected", 0777); 
        if (create != 0) {
            perror("Error creating directory");
        } else {
            printf("Directory sisop_infected created successfully.\n");
        }
        exit(EXIT_SUCCESS);
    } else if (mkdirSisop > 0) {
        mkdirQuarantine = fork();
        
        if (mkdirQuarantine == 0) {
            int create = mkdir("quarantine", 0777); 
            if (create != 0) {
                perror("Error creating directory");
            } else {
                printf("Directory quarantine created successfully.\n");
            }
            exit(EXIT_SUCCESS);
        } else if (mkdirQuarantine < 0) {
            perror("Error forking child process 2");
        }
    } else {
        perror("Error forking child process 1");
    }
}

void downloadFile() {
    pid_t wget = fork();
    if (wget == 0){
        execl("/usr/bin/wget", "wget", "-O", "extensions.csv", "https://drive.google.com/u/0/uc?id=1gIhwR7JLnH5ZBljmjkECzi8tIlW8On_5&export=download", NULL );
        exit(EXIT_SUCCESS);
    } else if (wget < 0) {
        perror("Fork failed");
        exit(EXIT_FAILURE);
    } else {
        int status; 
        waitpid(wget, &status, 0);
    }
}

void move_file(const char *source_path, const char *destination_dir) {
    pid_t pid = fork();
    if (pid == 0) {
        execlp("mv", "mv", source_path, destination_dir, NULL);
        perror("Error executing execlp");
        exit(EXIT_FAILURE); // Jika execlp gagal
    } else if (pid > 0) {
        int status;
        waitpid(pid, &status, 0);
    } else {
        perror("Error forking child process");
    }
}

void logVirusInfo(char *filename, char *action) {
    char *username = getlogin();

    pid_t pid = fork();
    if (pid == 0) {
        char timestamp[20];
        time_t t;
        struct tm *tm_info;

        time(&t);
        tm_info = localtime(&t);

        strftime(timestamp, 20, "%d-%m-%y:%H-%M-%S", tm_info);

        char log_entry[256];
        snprintf(log_entry, sizeof(log_entry), "[%s][%s] - %s - %s", username, timestamp, filename, action);

        FILE *log_file = fopen("virus.log", "a");
        if (log_file != NULL) {
            fprintf(log_file, "%s\n", log_entry);
            fclose(log_file);
            exit(EXIT_SUCCESS);
        } else {
            perror("Error opening virus.log");
            exit(EXIT_FAILURE);
        }
    } else if (pid > 0) {
        int status;
        waitpid(pid, &status, 0);
    } else {
        perror("Error forking child process");
    }
}

void matchFiles() {
    char line[256];
    FILE *extensions_file = fopen("extensions.csv", "r");
    if (!extensions_file) {
        perror("Error opening extensions.csv");
    }

    while (fgets(line, sizeof(line), extensions_file)) {
        line[strcspn(line, "\n")] = 0;  

        DIR *dir;
        struct dirent *entry;

        dir = opendir("sisop_infected/");
        if (!dir) {
            perror("Error opening directory sisop_infected/");
        }

        while ((entry = readdir(dir))) {
            if (entry->d_type == DT_REG) { 
                char *ext = strrchr(entry->d_name, '.');
                if (ext != NULL && strstr(line, ext) != NULL) {
                    char source_path[300]; 
                    snprintf(source_path, sizeof(source_path), "sisop_infected/%s", entry->d_name);
                    move_file(source_path, "quarantine/");

                    // Mencatat informasi di virus.log
                    logVirusInfo(entry->d_name, "Moved to quarantine");
                }
            }
        }

        closedir(dir);
    }

    fclose(extensions_file);
}

void dummyFiles() {
    int pid = fork();
    if (pid == 0) {
        // Child process
        system("echo hello > sisop_infected/dsakdlmsa*.RAPE");
    } else if (pid > 0) {
        // Parent process
        int status;
        waitpid(pid, &status, 0);
    }
}

    void daemonize() {
        pid_t pid, sid;
        pid = fork();

        if (pid < 0) {
            exit(EXIT_FAILURE);
        }

        if (pid > 0) {
            exit(EXIT_SUCCESS);
        }

        umask(0);
        sid = setsid();

        if (sid < 0) {
            exit(EXIT_FAILURE);
        }

        if ((chdir("/")) < 0) {
            exit(EXIT_FAILURE);
        }

        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);
    }

int main() {

    makeDirectory();
    downloadFile();
    dummyFiles();
    decryptExtensions();

    daemonize();
while(1){
    matchFiles();
    sleep(1);
}

    return 0;
}
